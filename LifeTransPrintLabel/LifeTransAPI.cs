﻿using Common;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web;
using System.Windows.Forms;

namespace LifeTransPrintLabel
{
    public partial class LifeTransAPI : Form
    {
        public LifeTransAPI()
        {
            InitializeComponent();
        }

        private void txtBarcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 13)
            {
                string szBarcode = txtBarcode.Text.Trim();

                lbBarcode.Text = txtBarcode.Text;

                txtBarcode.Text = string.Empty;

                txtBarcode.Focus();

                LifeTransPostData data = GetDeliveryRequest(szBarcode);
                CallApi(data);
            }
        }

        // get data from deliveryRequest
        private LifeTransPostData GetDeliveryRequest(string checkNumber)
        {
            string query = string.Format(@"select order_number, receive_contact, subpoena_category, collection_money, invoice_desc,
                    receive_tel1, receive_tel2, arrive_email, receive_zip, receive_city, receive_area, receive_address
                    from tcDeliveryRequests where check_number = '{0}' order by request_id desc", checkNumber);

            DataTable dtDeliveryRequest = DbAdaptor.GetDataTableBySql(query);
            DataRow drDeliveryRequest = dtDeliveryRequest.Rows[0];

            int orderMoney;
            int.TryParse(drDeliveryRequest["collection_money"].ToString(), out orderMoney);

            LifeTransPostData data = new LifeTransPostData
            {
                member_account = drDeliveryRequest["receive_contact"].ToString(),
                company_orderno = checkNumber,
                //service_type = drDeliveryRequest["subpoena_category"].ToString(),
                order_money = orderMoney,
                memo = drDeliveryRequest["invoice_desc"].ToString(),
                order_user = drDeliveryRequest["receive_contact"].ToString(),
                zip_code = GetZip(drDeliveryRequest["receive_city"].ToString(), drDeliveryRequest["receive_area"].ToString()),
                order_user_email = drDeliveryRequest["arrive_email"].ToString(),
                order_user_phone = drDeliveryRequest["receive_tel1"].ToString(),
                order_user_mobile = drDeliveryRequest["receive_tel2"].ToString(),
                address = string.Format("{0}{1}{2}", drDeliveryRequest["receive_city"].ToString(), drDeliveryRequest["receive_area"].ToString(), drDeliveryRequest["receive_address"].ToString())
            };

            if (data.order_money > 0)
                data.service_type = 1;  // 

            return data;
        }

        /// <summary>
        /// 取得郵遞區號
        /// </summary>
        /// <param name="city"></param>
        /// <param name="area"></param>
        /// <returns></returns>
        private string GetZip(string city, string area)
        {
            string query = string.Format(@"SELECT top 1 zip FROM ttArriveSitesScattered__
where post_city = '{0}' and post_area = '{1}'", city, area);

            DataTable dt = DbAdaptor.GetDataTableBySql(query);

            if(dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                return dr["zip"].ToString();
            }

            return string.Empty;
        }

        // call life trans api
        private void CallApi(LifeTransPostData data)
        {
            string url = ConfigurationManager.AppSettings["ApiUrl"];

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            string queryParam = string.Empty;

            PropertyInfo[] properties = typeof(LifeTransPostData).GetProperties();
            foreach (PropertyInfo property in properties)
            {
                string value = property.GetValue(data).ToString();
                queryParam += string.Format("&{0}={1}", property.Name, value);
            }

            queryParam = queryParam.Substring(1);

            byte[] byteArray = Encoding.UTF8.GetBytes(queryParam);
            using (Stream reqStream = request.GetRequestStream())
            {
                reqStream.Write(byteArray, 0, byteArray.Length);
            }
            string responseStr = "";

            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                {
                    responseStr = sr.ReadToEnd();
                }
            }

            SaveReaponseData(responseStr);
        }

        private void SaveReaponseData(string response)
        {
            NameValueCollection collectionResponse = HttpUtility.ParseQueryString(response);

            string checkNumber = collectionResponse["company_orderno"];
            string resultCode = collectionResponse["order_result"];
            string companyOrderNo = collectionResponse["company_orderno"];
            string shipNo = collectionResponse["ship_no"];
            string sysOrderNo = collectionResponse["sys_orderno"];
            string distsiteId = collectionResponse["distsite_id"];
            string pointName = collectionResponse["point_name"];
            string errorMsg = collectionResponse["error_msg"];

            string insertQuery = string.Format(@"insert into trans_life_trans (check_number, order_result, company_orderno, ship_no, sys_orderno, distsite_id, point_name, error_msg, cdate)
values ('{0}', {1}, '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}')", checkNumber, resultCode, companyOrderNo, shipNo,
sysOrderNo, distsiteId, pointName, errorMsg, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

            int effectRow = DbAdaptor.ExecuteNonQuery(insertQuery);

            if (effectRow > 0 && resultCode == "1")
            {
                lbBarcode.Text = string.Format("打單成功!\r\n託運編號\r\n{0}", shipNo);
            }
            else
            {
                lbBarcode.Text = string.Format("打單失敗\r\n錯誤訊息\r\n{0}", errorMsg);
            }
        }
    }
}
