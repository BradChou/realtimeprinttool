﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LifeTransPrintLabel
{
    public class DeliveryRequestEntity
    {
        public string ReceiveName { get; set; }

        public string OrderNumber { get; set; }

        public string SubpoenaCategory { get; set; }

        public int CollectionMoney { get; set; }

        public string Memo { get; set; }

        public string RecieveName { get; set; }

        public string ReceivePhone { get; set; }

        public string RecieveMobile { get; set; }

        public string RecieveEmail { get; set; }

        public string RecieveArea { get; set; }

        public string RecieveCity { get; set; }

        public string RecieveRoad { get; set; }

        public string RecieveZip { get; set; }
    }
}
