﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LifeTransPrintLabel
{
    public class TcDeliveryRequest
    {
        public decimal RequestId { get; set; } // request_id (Primary key)
        public string PricingType { get; set; } // pricing_type (length: 2)

        /// <summary>
        /// 客戶代碼
        /// </summary>
        public string CustomerCode { get; set; } // customer_code (length: 20)

        /// <summary>
        /// 貨號
        /// </summary>
        public string CheckNumber { get; set; } // check_number (length: 20)

        /// <summary>
        /// 類型
        /// </summary>
        public string CheckType { get; set; } // check_type (length: 10)

        /// <summary>
        /// 訂單編號
        /// </summary>
        public string OrderNumber { get; set; } // order_number (length: 32)

        /// <summary>
        /// 收件人代碼
        /// </summary>
        public string ReceiveCustomerCode { get; set; } // receive_customer_code (length: 20)

        /// <summary>
        /// 傳票類別
        /// </summary>
        public string SubpoenaCategory { get; set; } // subpoena_category (length: 10)

        /// <summary>
        /// 收件人電話1
        /// </summary>
        public string ReceiveTel1 { get; set; } // receive_tel1 (length: 20)

        /// <summary>
        /// 收件人電話1分機
        /// </summary>
        public string ReceiveTel1Ext { get; set; } // receive_tel1_ext (length: 10)

        /// <summary>
        /// 收件人電話2
        /// </summary>
        public string ReceiveTel2 { get; set; } // receive_tel2 (length: 20)

        /// <summary>
        /// 收件人
        /// </summary>
        public string ReceiveContact { get; set; } // receive_contact (length: 40)

        /// <summary>
        /// 收件人郵遞區號
        /// </summary>
        public string ReceiveZip { get; set; } // receive_zip (length: 6)

        /// <summary>
        /// 收件人城市
        /// </summary>
        public string ReceiveCity { get; set; } // receive_city (length: 10)

        /// <summary>
        /// 收件人地區
        /// </summary>
        public string ReceiveArea { get; set; } // receive_area (length: 10)

        /// <summary>
        /// 收件人地址
        /// </summary>
        public string ReceiveAddress { get; set; } // receive_address (length: 100)

        /// <summary>
        /// 配送站所
        /// </summary>
        public string AreaArriveCode { get; set; } // area_arrive_code (length: 15)

        /// <summary>
        /// 是否到站所
        /// </summary>
        public bool? ReceiveByArriveSiteFlag { get; set; } // receive_by_arrive_site_flag

        /// <summary>
        /// 站所地址?
        /// </summary>
        public string ArriveAddress { get; set; } // arrive_address (length: 100)

        /// <summary>
        /// 件數
        /// </summary>
        public int? Pieces { get; set; } // pieces

        /// <summary>
        /// 板數
        /// </summary>
        public int? Plates { get; set; } // plates

        /// <summary>
        /// 材積
        /// </summary>
        public int? Cbm { get; set; } // cbm

        /// <summary>
        /// 材積大小 (關連tbCbmSize.id )
        /// </summary>
        public int? CbmSize { get; set; } // CbmSize

        /// <summary>
        /// 收貨款
        /// </summary>
        public int? CollectionMoney { get; set; } // collection_money

        /// <summary>
        /// 到貨運費
        /// </summary>
        public int? ArriveToPayFreight { get; set; } // arrive_to_pay_freight
        public int? ArriveToPayAppend { get; set; } // arrive_to_pay_append

        /// <summary>
        /// 寄件人
        /// </summary>
        public string SendContact { get; set; } // send_contact (length: 40)

        /// <summary>
        /// 寄件人電話
        /// </summary>
        public string SendTel { get; set; } // send_tel (length: 40)

        /// <summary>
        /// 寄件人郵遞區號
        /// </summary>
        public string SendZip { get; set; } // send_zip (length: 6)

        /// <summary>
        /// 寄件人城市
        /// </summary>
        public string SendCity { get; set; } // send_city (length: 10)

        /// <summary>
        /// 寄件人地區
        /// </summary>
        public string SendArea { get; set; } // send_area (length: 10)

        /// <summary>
        /// 寄件人地址
        /// </summary>
        public string SendAddress { get; set; } // send_address (length: 50)

        /// <summary>
        /// 是否捐贈發票
        /// </summary>
        public bool? DonateInvoiceFlag { get; set; } // donate_invoice_flag

        /// <summary>
        /// 是否為電子發票
        /// </summary>
        public bool? ElectronicInvoiceFlag { get; set; } // electronic_invoice_flag

        /// <summary>
        /// 全NULL
        /// </summary>
        public string UniformNumbers { get; set; } // uniform_numbers (length: 8)

        /// <summary>
        /// 全NULL
        /// </summary>
        public string ArriveMobile { get; set; } // arrive_mobile (length: 20)

        /// <summary>
        /// 全NULL
        /// </summary>
        public string ArriveEmail { get; set; } // arrive_email (length: 50)

        /// <summary>
        /// 發票備忘錄
        /// </summary>
        public string InvoiceMemo { get; set; } // invoice_memo (length: 100)

        /// <summary>
        /// 發票說明
        /// </summary>
        public string InvoiceDesc { get; set; } // invoice_desc (length: 200)

        /// <summary>
        /// 產品類別
        /// </summary>
        public string ProductCategory { get; set; } // product_category (length: 10)

        /// <summary>
        /// 全NULL or 0
        /// </summary>
        public string SpecialSend { get; set; } // special_send (length: 10)

        /// <summary>
        /// 到達日期
        /// </summary>
        public DateTime? ArriveAssignDate { get; set; } // arrive_assign_date

        /// <summary>
        /// 期間
        /// </summary>
        public string TimePeriod { get; set; } // time_period (length: 10)

        /// <summary>
        /// 是否開收據
        /// </summary>
        public bool? ReceiptFlag { get; set; } // receipt_flag

        /// <summary>
        /// 貨盤是否回收
        /// </summary>
        public bool? PalletRecyclingFlag { get; set; } // pallet_recycling_flag

        /// <summary>
        /// 棧板種類( 關連 tbItemCodes)
        /// </summary>
        public string PalletType { get; set; } // Pallet_type (length: 2)

        /// <summary>
        /// 配商代碼
        /// </summary>
        public string SupplierCode { get; set; } // supplier_code (length: 3)

        /// <summary>
        /// 配商名稱
        /// </summary>
        public string SupplierName { get; set; } // supplier_name (length: 20)

        /// <summary>
        /// 配商日期
        /// </summary>
        public DateTime? SupplierDate { get; set; } // supplier_date

        /// <summary>
        /// 收據單號
        /// </summary>
        public int? ReceiptNumbe { get; set; } // receipt_numbe

        /// <summary>
        /// 配商費用
        /// </summary>
        public int? SupplierFee { get; set; } // supplier_fee
        public int? CsectionFee { get; set; } // csection_fee

        /// <summary>
        /// 遠程費用
        /// </summary>
        public int? RemoteFee { get; set; } // remote_fee

        /// <summary>
        /// 合計費用
        /// </summary>
        public int? TotalFee { get; set; } // total_fee

        /// <summary>
        /// 打印時間
        /// </summary>
        public DateTime? PrintDate { get; set; } // print_date

        /// <summary>
        /// 是否打印
        /// </summary>
        public bool? PrintFlag { get; set; } // print_flag

        /// <summary>
        /// 結帳關門時間
        /// </summary>
        public DateTime? CheckoutCloseDate { get; set; } // checkout_close_date

        /// <summary>
        /// 是否加入移倉
        /// </summary>
        public bool? AddTransfer { get; set; } // add_transfer

        /// <summary>
        /// 子貨號
        /// </summary>
        public string SubCheckNumber { get; set; } // sub_check_number (length: 3)

        /// <summary>
        /// 進口隨機碼
        /// </summary>
        public string ImportRandomCode { get; set; } // import_randomCode (length: 50)

        /// <summary>
        /// 關門隨機碼
        /// </summary>
        public string CloseRandomCode { get; set; } // close_randomCode (length: 10)

        /// <summary>
        /// 轉板
        /// </summary>
        public bool? TurnBoard { get; set; } // turn_board

        /// <summary>
        /// 上樓
        /// </summary>
        public bool? Upstairs { get; set; } // upstairs

        /// <summary>
        /// 是否配送困難
        /// </summary>
        public bool? DifficultDelivery { get; set; } // difficult_delivery

        /// <summary>
        /// 轉板費用
        /// </summary>
        public int? TurnBoardFee { get; set; } // turn_board_fee

        /// <summary>
        /// 上樓費用
        /// </summary>
        public int? UpstairsFee { get; set; } // upstairs_fee

        /// <summary>
        /// 困難費用
        /// </summary>
        public int? DifficultFee { get; set; } // difficult_fee

        /// <summary>
        /// 取消日期
        /// </summary>
        public DateTime? CancelDate { get; set; } // cancel_date

        /// <summary>
        /// 使用者
        /// </summary>
        public string Cuser { get; set; } // cuser (length: 20)

        /// <summary>
        /// 日期
        /// </summary>
        public DateTime? Cdate { get; set; } // cdate

        /// <summary>
        /// 使用者
        /// </summary>
        public string Uuser { get; set; } // uuser (length: 20)

        /// <summary>
        /// 日期
        /// </summary>
        public DateTime? Udate { get; set; } // udate

        /// <summary>
        /// HCT狀態：0, 1, 2, 3, 4, 5
        /// </summary>
        public int? HcTstatus { get; set; } // HCTstatus

        /// <summary>
        /// 回板託運單
        /// </summary>
        public bool? IsPallet { get; set; } // is_pallet

        /// <summary>
        /// 托盤需求ID
        /// </summary>
        public long? PalletRequestId { get; set; } // pallet_request_id

        /// <summary>
        /// 0：棧板運輸  1：零擔運輸
        /// </summary>
        public bool? LessThanTruckload { get; set; } // Less_than_truckload

        /// <summary>
        /// 配送商(全速配、宅配通) (關連 tbItemCodes where code_bclass = &apos;7&apos; and code_sclass=&apos;distributor&apos;)
        /// </summary>
        public string Distributor { get; set; } // Distributor (length: 50)

        /// <summary>
        /// 配送溫層 (01:一般 02:冷凍 03:冷藏) 關連tbItemCode
        /// </summary>
        public string Temperate { get; set; } // temperate (length: 10)

        /// <summary>
        /// 配送類型 D:正物流  R:逆物流
        /// </summary>
        public string DeliveryType { get; set; } // DeliveryType (length: 10)

        /// <summary>
        /// 材積長
        /// </summary>
        public double? CbmLength { get; set; } // cbmLength

        /// <summary>
        /// 材積寬
        /// </summary>
        public double? CbmWidth { get; set; } // cbmWidth

        /// <summary>
        /// 材積高
        /// </summary>
        public double? CbmHeight { get; set; } // cbmHeight

        /// <summary>
        /// 材積重
        /// </summary>
        public double? CbmWeight { get; set; } // cbmWeight

        /// <summary>
        /// 材積內容
        /// </summary>
        public double? CbmCont { get; set; } // cbmCont

        /// <summary>
        /// 袋號
        /// </summary>
        public string Bagno { get; set; } // bagno (length: 20)

        /// <summary>
        /// 產品編號
        /// </summary>
        public string ArticleNumber { get; set; } // ArticleNumber (length: 20)

        /// <summary>
        /// 出貨平台
        /// </summary>
        public string SendPlatform { get; set; } // SendPlatform (length: 20)

        /// <summary>
        /// 品名
        /// </summary>
        public string ArticleName { get; set; } // ArticleName (length: 20)

        /// <summary>
        /// 來回件
        /// </summary>
        public bool? RoundTrip { get; set; } // round_trip

        /// <summary>
        /// 出貨日
        /// </summary>
        public DateTime? ShipDate { get; set; } // ship_date

        /// <summary>
        /// 振興卷金額
        /// </summary>
        public int? VoucherMoney { get; set; } // VoucherMoney

        /// <summary>
        /// 現金金額
        /// </summary>
        public int? CashMoney { get; set; } // CashMoney

        /// <summary>
        /// 集貨區分袋種
        /// </summary>
        public string PickUpGoodType { get; set; } // pick_up_good_type (length: 10)

        /// <summary>
        /// 最新歷程掃讀時間
        /// </summary>
        public DateTime? LatestScanDate { get; set; } // latest_scan_date

        /// <summary>
        /// 第一次正常配交時間
        /// </summary>
        public DateTime? DeliveryCompleteDate { get; set; } // delivery_complete_date
        public string PicPath { get; set; } // pic_path
        public string SendStationScode { get; set; } // send_station_scode (length: 15)
        public bool? IsWriteOff { get; set; } // Is_write_off
        public string ReturnCheckNumber { get; set; } // return_check_number (length: 20)

        /// <summary>
        /// 運價
        /// </summary>
        public int? Freight { get; set; } // freight

        /// <summary>
        /// 最新到著時間
        /// </summary>
        public DateTime? LatestDestDate { get; set; } // latest_dest_date

        /// <summary>
        /// 最新配送時間
        /// </summary>
        public DateTime? LatestDeliveryDate { get; set; } // latest_delivery_date

        /// <summary>
        /// 最新配達時間
        /// </summary>
        public DateTime? LatestArriveOptionDate { get; set; } // latest_arrive_option_date

        /// <summary>
        /// 最新到著司機編號
        /// </summary>
        public string LatestDestDriver { get; set; } // latest_dest_driver (length: 10)

        /// <summary>
        /// 最新配送司機編號
        /// </summary>
        public string LatestDeliveryDriver { get; set; } // latest_delivery_driver (length: 10)

        /// <summary>
        /// 最新配達司機編號
        /// </summary>
        public string LatestArriveOptionDriver { get; set; } // latest_arrive_option_driver (length: 10)

        /// <summary>
        /// 最新配達項目
        /// </summary>
        public string LatestArriveOption { get; set; } // latest_arrive_option (length: 20)

        /// <summary>
        /// 應配日(ship_date + 1/當日有掃配送/超過ship_date + 1沒配送也沒銷單)
        /// </summary>
        public DateTime? DeliveryDate { get; set; } // delivery_date
        public string PaymentMethod { get; set; } // payment_method (length: 15)
        public string CatchCbmPicPathFromS3 { get; set; } // catch_cbm_pic_path_from_S3
        public string CatchTaichungCbmPicPathFromS3 { get; set; } // catch_taichung_cbm_pic_path_from_S3

        /// <summary>
        /// 最新掃描項目
        /// </summary>
        public string LatestScanItem { get; set; } // latest_scan_item (length: 2)

        /// <summary>
        /// 最新掃描配達項目
        /// </summary>
        public string LatestScanArriveOption { get; set; } // latest_scan_arrive_option (length: 20)

        /// <summary>
        /// 最新掃描司機編號
        /// </summary>
        public string LatestScanDriverCode { get; set; } // latest_scan_driver_code (length: 10)
        public string LatestDestExceptionOption { get; set; } // latest_dest_exception_option (length: 20)
        public bool? HolidayDelivery { get; set; } // holiday_delivery

        public TcDeliveryRequest()
        {
            IsPallet = false;
            PalletRequestId = 0;
            LessThanTruckload = false;
            DeliveryType = "D";
            RoundTrip = false;
        }

    }
}
