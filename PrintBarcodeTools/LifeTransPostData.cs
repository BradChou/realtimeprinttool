﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LifeTransPrintLabel
{
    public class LifeTransPostData
    {
        public LifeTransPostData()
        {
            order_action = "New";  // 可更新訂單
            account = ConfigurationManager.AppSettings["account"];
            order_type = 17;  // 大榮
            service_type = 3;  // 純取貨
        }

        public string order_action { get; set; }

        public string account { get; set; }

        public string member_account { get; set; }

        public string company_orderno { get; set; }

        public int order_type { get; set; }

        public int service_type { get; set; }

        public int order_money { get; set; }

        public string memo { get; set; }

        public string order_user { get; set; }

        public string order_user_email { get; set; }

        public string order_user_phone { get; set; }

        public string order_user_mobile { get; set; }

        public string zip_code { get; set; }

        public string address { get; set; }
    }
}
