﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Net;
using System.Windows.Forms;
using PdfPrintingNet;
using LifeTransPrintLabel;
using Common;
using System.Reflection;
using System.IO;
using System.Collections.Specialized;
using System.Web;

namespace PrintBarcodeTools
{
    public partial class fmBarcodePrint : Form
    {
        public fmBarcodePrint()
        {
            InitializeComponent();
        }

        private void TxtBarcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                //資料判斷
                string barcode = this.TxtBarcode.Text.Trim();

                this.TxtBarcode.Text = string.Empty;

                this.TxtBarcode.Focus();


                //取得設定檔內容
                string Printer1 = ConfigurationManager.AppSettings["Printer1"];
                string Printer2 = ConfigurationManager.AppSettings["Printer2"];
                string Printer3 = ConfigurationManager.AppSettings["Printer3"];

                string semiUrl = "";
                bool success = true;
                if (comboBox1.SelectedIndex == -1)
                {
                    return;
                }
                if (comboBox1.SelectedIndex == 0)
                {
                    semiUrl = ConfigurationManager.AppSettings["PelicanUrl"] + "&date=null";
                }
                else if(comboBox1.SelectedIndex == 1)
                {
                    success = CallLifeTrans(barcode);
                    semiUrl = ConfigurationManager.AppSettings["LifeTransUrl"];
                }

                if (!success)
                    return;

                //組成網址                
                string Url = string.Format(semiUrl, barcode);

                WebClient webClient = new WebClient();

                byte[] bytesDownload = webClient.DownloadData(Url);

                var pdfPrint = new PdfPrint("Winjet Inc.", "5b5d26533151575650245e5a412b57");

                pdfPrint.PrinterName = Printer3;

                pdfPrint.Print(bytesDownload, "", true);
            }
        }

        #region 錸乾
        private bool CallLifeTrans(string checkNumber)
        {
            var data = GetDeliveryRequest(checkNumber);
            return CallApi(data);
        }

        private LifeTransPostData GetDeliveryRequest(string checkNumber)
        {
            string query = string.Format(@"select order_number, receive_contact, subpoena_category, collection_money, invoice_desc,
                    receive_tel1, receive_tel2, arrive_email, receive_zip, receive_city, receive_area, receive_address
                    from tcDeliveryRequests where check_number = '{0}' order by request_id desc", checkNumber);

            DataTable dtDeliveryRequest = DbAdaptor.GetDataTableBySql(query);
            DataRow drDeliveryRequest = dtDeliveryRequest.Rows[0];

            int orderMoney;
            int.TryParse(drDeliveryRequest["collection_money"].ToString(), out orderMoney);

            LifeTransPostData data = new LifeTransPostData
            {
                member_account = drDeliveryRequest["receive_contact"].ToString(),
                company_orderno = checkNumber,
                //service_type = drDeliveryRequest["subpoena_category"].ToString(),
                order_money = orderMoney,
                memo = drDeliveryRequest["invoice_desc"].ToString(),
                order_user = drDeliveryRequest["receive_contact"].ToString(),
                zip_code = GetZip(drDeliveryRequest["receive_city"].ToString(), drDeliveryRequest["receive_area"].ToString()),
                order_user_email = drDeliveryRequest["arrive_email"].ToString(),
                order_user_phone = drDeliveryRequest["receive_tel1"].ToString(),
                order_user_mobile = drDeliveryRequest["receive_tel2"].ToString(),
                address = string.Format("{0}{1}{2}", drDeliveryRequest["receive_city"].ToString(), drDeliveryRequest["receive_area"].ToString(), drDeliveryRequest["receive_address"].ToString())
            };

            if (data.order_money > 0)
                data.service_type = 1;  // 

            return data;
        }

        /// <summary>
        /// 取得郵遞區號
        /// </summary>
        /// <param name="city"></param>
        /// <param name="area"></param>
        /// <returns></returns>
        private string GetZip(string city, string area)
        {
            string query = string.Format(@"SELECT top 1 zip FROM ttArriveSitesScattered__
where post_city = '{0}' and post_area = '{1}'", city, area);

            DataTable dt = DbAdaptor.GetDataTableBySql(query);

            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                return dr["zip"].ToString();
            }

            return string.Empty;
        }

        // call life trans api
        private bool CallApi(LifeTransPostData data)
        {
            string url = ConfigurationManager.AppSettings["LTApiUrl"];

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            string queryParam = string.Empty;

            PropertyInfo[] properties = typeof(LifeTransPostData).GetProperties();
            foreach (PropertyInfo property in properties)
            {
                string value = property.GetValue(data).ToString();
                queryParam += string.Format("&{0}={1}", property.Name, value);
            }

            queryParam = queryParam.Substring(1);

            byte[] byteArray = Encoding.UTF8.GetBytes(queryParam);
            using (Stream reqStream = request.GetRequestStream())
            {
                reqStream.Write(byteArray, 0, byteArray.Length);
            }
            string responseStr = "";

            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                {
                    responseStr = sr.ReadToEnd();
                }
            }

            return SaveReaponseData(responseStr);
        }

        private bool SaveReaponseData(string response)
        {
            NameValueCollection collectionResponse = HttpUtility.ParseQueryString(response);

            string checkNumber = collectionResponse["company_orderno"];
            string resultCode = collectionResponse["order_result"];
            string companyOrderNo = collectionResponse["company_orderno"];
            string shipNo = collectionResponse["ship_no"];
            string sysOrderNo = collectionResponse["sys_orderno"];
            string distsiteId = collectionResponse["distsite_id"];
            string pointName = collectionResponse["point_name"];
            string errorMsg = collectionResponse["error_msg"];

            string insertQuery = string.Format(@"insert into trans_life_trans (check_number, order_result, company_orderno, ship_no, sys_orderno, distsite_id, point_name, error_msg, cdate, print_date)
values ('{0}', {1}, '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}')", checkNumber, resultCode, companyOrderNo, shipNo,
sysOrderNo, distsiteId, pointName, errorMsg, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

            int effectRow = DbAdaptor.ExecuteNonQuery(insertQuery);

            if (effectRow > 0 && resultCode == "1")
            {
                //lbBarcode.Text = string.Format("打單成功!\r\n託運編號\r\n{0}", shipNo);
                return true;
            }
            else
            {
                MessageBox.Show("打單失敗\r\n錯誤訊息\r\n" + errorMsg);
            }
            return false;
        }
        #endregion

        private void fmBarcodePrint_Load(object sender, EventArgs e)
        {

        }

        
        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            ShowData showData = new ShowData();
            showData.Show();
            showData.dataGridView1.DataSource = CheckNumberData();
        }

        private DataTable CheckNumberData()
        {
            var start = dateTimePicker1.Value.ToString("yyyy/MM/dd");
            var end = dateTimePicker2.Value.AddDays(1).ToString("yyyy/MM/dd");

            string sql = string.Format(@"
            SELECT DISTINCT
            pelican_check_number as '轉運商貨號'
            ,c.receive_contact as '收件人姓名'
            ,c.receive_city + c.receive_area + c.receive_address as '收件人地址' 
            ,jf_check_number  as '全速配貨號'
            from pelican_request p
            join ttDeliveryScanLog t on p.pelican_check_number = t.check_number 
			join tcDeliveryRequests c on p.jf_request_id = c.request_id 
            where  driver_code = 'PP992' and scan_item = '7' and delivery_type = 'R' 
            and scan_date >= '{0}' and scan_date < '{1}'", start, end);

            DataTable table = DbAdaptor.GetDataTableBySql(sql);
            return table;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var start = dateTimePicker1.Value.ToString("yyyy/MM/dd");
            var end = dateTimePicker2.Value.AddDays(1).ToString("yyyy/MM/dd");

            string sql = string.Format(@"
            select distinct jf_request_id from Pelican_request p
            join ttDeliveryScanLog t on p.jf_check_number = t.check_number
            where driver_code = 'PP992' and scan_item = '7' and delivery_type = 'R'
            and scan_date >='{0}' and scan_date < '{1}'", start, end);

            DataTable table = DbAdaptor.GetDataTableBySql(sql);

            List<string> ids = new List<string>();

            if (table.Rows.Count > 0)
            {
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    string id = table.Rows[i]["jf_request_id"].ToString();
                    ids.Add(id);
                }
            }
            string outputIds = string.Join(",", ids);

            string Url = string.Format(ConfigurationManager.AppSettings["ReturnTagUrl"], outputIds);

            WebClient webClient = new WebClient();

            byte[] bytesDownload = webClient.DownloadData(Url);

            var pdfPrint = new PdfPrint("Winjet Inc.", "5b5d26533151575650245e5a412b57");

            pdfPrint.PrinterName = textBox1.Text;

            pdfPrint.Print(bytesDownload, "", true);
        }

    }
}
